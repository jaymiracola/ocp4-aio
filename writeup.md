# OCP - Bare Metal Single Node
The purpose of this initial write up will be a way to use existing tools to get an OCP 4.x install on one machine. There are several other write ups in the same vein but I will hopefully make this one in a way that makes less assumptions of your current OCP experience. Some linux skills _will_ be required :)

### Networking
- It should also be noted that the helper will setup DHCP,PXE, and more on the target network. Unless you want to configure DHCP options, the easiest way to get everything setup initially is by having everything on an isolated private network that has internet access.

### Required Compute
- Helper node
- bootstrap (temporary)
- OCP Machine

### Machine Specs
- 4 cores
- 16 Gb Ram
- 120 Gb storage


### Helper Node setup
- Install ansible and git
``` yum install ansible git -y ```
- Git clone the helper node repo
``` git clone https://github.com/RedHatOfficial/ocp4-helpernode.git ```
- Copy and edit the vars.yaml to your environment needs 
```
cd ocp4-helpernode
cp docs/examples/vars.yaml .
```
- Edit the variables for your environment. I will include an example in this repo that shows all but 1 work and master commented out. The playbook needs at least one worker to be present to execute. It will have not impact on the goal of having an all inclusive master/worker.
- run the helper node ansible command (with become as needed)
``` ansible-playbook -b -e @vars.yaml tasks/main.yml```
- Create your install-config.yaml. I have included a sample in this repo that will work for a single node installation. You will need to add _your_ pull secret and ssh key 
- Run the ignition command to create ignition files from your install-config.yaml. This will consume/ remove your install-config.yaml. You will need to state the directory of your config if you are not currently in it.
``` openshift-install create ignition-configs ```
- Copy, fix permissions, and set contexts on your newly created .ign files
```
cp *.ign /var/www/html/ignition/
restorecon -vR /var/www/html/
chmod 555 /var/www/html/ignition/*
```
- Power on the bootstrap machine. Wait for PXE to complete.
- Power on the Master/Worker. Wait for PXE to complete.

### Bootstrap
- SSH to the bootstrap using the key you provided previously.
- Watch the bootkube.service and wait for it to complete.
```journalctl -b -f -u bootkube.service```
- When bootstrap completes, you may power off and remove the machine
- Edit DNS entries located at ```/var/named/zonefile.db``` . You will need to make sure your DNS entries now point to the Master instead of the bootstrap machine.
```
api             IN      A       <master IP>
api-int         IN      A       <master IP>
```

- Restart bind (named)
``` systemctl restart named ```

### Master/ Worker setup
- From the helper node , scale etcd-quorum guard and replica sets with the included shell script.
- You can watch the completion of your node by using ```watch oc get co``` . Wait for all pods in all namespaces to complete.
- Now use ```openshift-install wait-for install-complete``` to verify installation and get login information.
- All done!
